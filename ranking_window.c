#include <gtk/gtk.h>
#include <stdlib.h>

#include "ranking_window.h"
#include "ranking_back_end.h"
#include "ranking_data.h"


void create_ranking_window (void){

    GtkWindow *window;
    window = (GtkWindow*)gtk_window_new (GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title (GTK_WINDOW (window), "Ranking");
    gtk_container_set_border_width (GTK_CONTAINER (window), 5);

    GtkBox *master_box;
    master_box = (GtkBox*)gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);

    RankingData data;
    data = create_RankingData_struct();

    if(read_ranking(data)){
        GtkBox *single_player_box;
        single_player_box = (GtkBox*)gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
        gtk_container_set_border_width (GTK_CONTAINER (single_player_box), 10);

        GtkLabel *single_player_label;
        single_player_label = (GtkLabel*)gtk_label_new("Wyniki gry jednosobowej\n");
        gtk_container_add (GTK_CONTAINER (single_player_box), (GtkWidget*)single_player_label);

        GtkLabel *single_result_label[5];
        char single_result_text[100];

        if( data->single_number == 0 ){
            sprintf(single_result_text, "Brak wynikow do wyswietlenia");
            single_result_label[1] = (GtkLabel*)gtk_label_new(single_result_text);
            gtk_container_add (GTK_CONTAINER (single_player_box), (GtkWidget*)single_result_label[1]);
        }
        for( int i=0; i < data->single_number; i++){
            sprintf(single_result_text, "%d\t%s\t%d", (i+1), data->single_name[i], data->single_score[i]);
            single_result_label[i] = (GtkLabel*)gtk_label_new(single_result_text);
            gtk_container_add (GTK_CONTAINER (single_player_box), (GtkWidget*)single_result_label[i]);
        }

        GtkBox *multi_player_box;
        multi_player_box = (GtkBox*)gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
        gtk_container_set_border_width (GTK_CONTAINER (multi_player_box), 10);

        GtkLabel *multi_player_label;
        multi_player_label = (GtkLabel*)gtk_label_new("Wyniki gry dwuosobowej\n");
        gtk_container_add (GTK_CONTAINER (multi_player_box), (GtkWidget*)multi_player_label);

        GtkLabel *multi_result_label[5];
        char multi_result_text[100];

        if( data->multi_number == 0 ){
            sprintf(multi_result_text, "Brak wynikow do wyswietlenia");
            multi_result_label[1] = (GtkLabel*)gtk_label_new(multi_result_text);
            gtk_container_add (GTK_CONTAINER (multi_player_box), (GtkWidget*)multi_result_label[1]);
        }
        for( int i=0; i < data->multi_number; i++){
            sprintf(multi_result_text, "%d\t%s\t%d", (i+1), data->multi_name[i], data->multi_score[i]);
            multi_result_label[i] = (GtkLabel*)gtk_label_new(multi_result_text);
            gtk_container_add (GTK_CONTAINER (multi_player_box), (GtkWidget*)multi_result_label[i]);
        }

        GtkLabel *result_exp;
        result_exp = (GtkLabel*)gtk_label_new("Nr\tImię\tPkt.");

        gtk_container_add (GTK_CONTAINER (master_box), (GtkWidget*)single_player_box);
        gtk_container_add (GTK_CONTAINER (master_box), (GtkWidget*)multi_player_box);
        gtk_container_add (GTK_CONTAINER (master_box), (GtkWidget*)result_exp);
    }
    else{
        GtkBox *player_box;
        player_box = (GtkBox*)gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
        gtk_container_set_border_width (GTK_CONTAINER (player_box), 10);

        GtkLabel *player_label;
        player_label = (GtkLabel*)gtk_label_new("Brak wynikow do wyswietlenia");
        gtk_container_add (GTK_CONTAINER (player_box), (GtkWidget*)player_label);

        gtk_container_add (GTK_CONTAINER (master_box),(GtkWidget*) player_box);

    }
    free( data );
    gtk_container_add (GTK_CONTAINER (window), (GtkWidget*)master_box);
    gtk_widget_show_all ((GtkWidget*)window);
}
