#include <gtk/gtk.h>
#include <stdlib.h>

#include "ranking_window.h"
#include "from_main_to_game_data.h"
#include "game_window.h"
#include "game_back_end.h"
#include "game_end_window.h"



void create_game_window( BoardData *data ){
    GtkWindow *window;
    window = (GtkWindow*)gtk_window_new (GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title (GTK_WINDOW (window), "Memory game");
    g_signal_connect (window, "destroy", G_CALLBACK (game_quit), data); //Dopisać freeloc funkcję
    gtk_container_set_border_width (GTK_CONTAINER (window), 10);
    data->game_window=window;


    GtkWidget *master_box;
    master_box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);

    GtkWidget *label_box;
    GtkWidget *label_box1, *label_box2;
    label_box = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
    label_box1 = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
    label_box2 = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
    gtk_box_set_homogeneous(GTK_BOX(label_box), TRUE);

    GtkWidget *labelM;
    labelM = gtk_label_new("Twój wynik: 0");
    gtk_box_pack_start(GTK_BOX(label_box1), labelM, TRUE, FALSE, 0);

    GtkWidget *labelE;
    labelE = gtk_label_new("Wynik drugiego gracza: 0");
    gtk_box_pack_start(GTK_BOX(label_box2), labelE, TRUE, FALSE, 0);

    gtk_box_pack_start(GTK_BOX (label_box), label_box1, TRUE, FALSE, 0);
    if(data->mode == 'A' || data->mode == 'B'){
        gtk_container_add (GTK_CONTAINER (label_box), label_box2);
    }
    gtk_container_add (GTK_CONTAINER (master_box), label_box);

    GtkWidget *game_box;
    GtkGrid *grid;

    game_box = gtk_event_box_new ();
    grid = (GtkGrid*)gtk_grid_new ();
    gtk_grid_set_row_spacing (grid, 5);
    gtk_grid_set_column_spacing (grid, 5);


    data->label[0]=labelM;
    data->label[1]=labelE;


    GError *error = NULL;
    data->blank = gdk_pixbuf_new_from_file("Pictures/blank.png", &error);
    data->color = gdk_pixbuf_new_from_file("Pictures/card.png", &error);

    char image_name[100];
    for(int i=0; i<data->size_half; i++){
        sprintf(image_name, "Pictures/%d/%d.png", data->icon_set ,i);
        printf("Image[%d] from file: ""%s""\n", i, image_name );
        data->image[i] = gdk_pixbuf_new_from_file (image_name, &error);
    }
    printf("\n");

    for (int i=0; i<data->size_full; i++){
        data->card[i] = (GtkImage*) gtk_image_new_from_pixbuf (data->color);
    }

    for (int x=0; x<data->size_x; x++){
        for (int y=0; y<data->size_y; y++){
            gtk_grid_attach(GTK_GRID(grid), (GtkWidget*)data->card[x+(y*data->size_x)], x, y, 1, 1);
        }
    }

    gtk_container_add (GTK_CONTAINER (game_box), (GtkWidget*)grid);

    g_signal_connect (G_OBJECT (game_box),
                      "button_press_event",
                      G_CALLBACK (button_press_callback),
                      data);


    gtk_container_add (GTK_CONTAINER (master_box), game_box);
    gtk_container_add (GTK_CONTAINER (window), master_box);

    if(data->mode == 'A' || data->mode == 'B'){
        g_timeout_add(100, get_second_player_move, data);
    }

    gtk_widget_show_all ( (GtkWidget*)window );
}
