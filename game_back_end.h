#include "game_data.h"



void board_generator(int size, int tab[size]);
void board_print(BoardData *data);
void button_press_callback (GtkWidget      *event_box,
                            GdkEventButton *event,
                            gpointer       data_in);
gboolean get_second_player_move(gpointer data);
void send_board (BoardData *data);
bool get_board (BoardData *data);
