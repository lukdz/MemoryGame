#ifndef game_data_h
#define game_data_h

#include <gtk/gtk.h>

#include "fifo.h"

typedef struct BoardData_type BoardData;

struct BoardData_type{
    int load;

    char mode;
    int size_full;
    int size_half;
    int size_x;
    int size_y;
    int icon_set;
    GtkEntry *entry;

    PipesPtr channel;

    GtkWindow *game_window;
    GtkWindow *game_end_window;
    GtkWidget *label[2];
    GtkImage *card[40];

    bool quit;
    char name[20];
    int cards_left;
    bool turn;
    bool taken[40];
    GdkPixbuf *blank;
    GdkPixbuf *color;
    GdkPixbuf *image[20];
    int clicked[2];
    int score[2];
    int x;
    int y;
    int tab[40];
};
#endif // game_data_h
