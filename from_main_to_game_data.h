typedef struct main_game_type *MGData;

struct main_game_type{
    char mode;
    int board_size;
    int icon_set;
};
