#include <gtk/gtk.h>
#include <stdlib.h>

#include "game_end_window.h"
#include "ranking_back_end.h"



void terminate_get_second_player_move( BoardData *data ){
    char text[10]={"Q"};
    sendStringToPipe(data->channel, text);
    closePipes( data->channel );
    data->quit = true;
}

void game_quit( GtkWidget* widget,  BoardData *data ){
    if ( data->game_window != NULL ){
        gtk_widget_destroy( (GtkWidget*)data->game_window );
        data->game_window = NULL;
        if( data->game_end_window != NULL){
            gtk_widget_destroy( (GtkWidget*)data->game_end_window );
            data->game_end_window = NULL;
        }
        if( data->mode == 'A' || data->mode == 'B' ){
            terminate_get_second_player_move( data );
        }
        free( data );
    }
}


void save_result (GtkWidget* widget, BoardData *data){
    const gchar *text;
    text = gtk_entry_get_text (data->entry);
    printf("Imię: %s\n", text);

    add_to_ranking(data->score[0], text, data->mode);

    game_quit(widget, data);
}




void results_display(BoardData *data){
    GtkWindow *window;
    window = (GtkWindow*)gtk_window_new (GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title (GTK_WINDOW (window), "KONIEC");
    g_signal_connect (window, "destroy", G_CALLBACK (game_quit), data);
    gtk_container_set_border_width (GTK_CONTAINER (window), 10);
    data->game_end_window=window;

    GtkBox* master_box;
    master_box = (GtkBox*)gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);

    if( data->mode == 'S' || ((data->mode == 'A' || data->mode == 'B') && data->score[0] < data->score[1]) ){
        GtkLabel *result_label;
        result_label = (GtkLabel*)gtk_label_new("Gratulacje wygrałeś!");
        gtk_box_pack_start(GTK_BOX(master_box), (GtkWidget*)result_label, TRUE, TRUE, 0);

        int poz;
        poz = check_ranking_position(data->score[0], data->mode);
        if( poz != -1){
            char text[100];
            sprintf(text, "\nTwój wynik ma %d pozycje w rankingu.", (poz+1));
            GtkLabel *result_number_label;
            result_number_label = (GtkLabel*)gtk_label_new(text);
            gtk_container_add (GTK_CONTAINER (master_box), (GtkWidget*)result_number_label);

            //GtkEntry *entry;
            GtkEntryBuffer *buffer;
            buffer = gtk_entry_buffer_new ("Imię", 4);
            data->entry = (GtkEntry*) gtk_entry_new_with_buffer (buffer);
            gtk_box_pack_start (GTK_BOX(master_box), (GtkWidget*)data->entry, TRUE, TRUE, 0);
            //data->entry=entry;


            GtkBox *button_box;
            button_box = (GtkBox*)gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
            gtk_container_add (GTK_CONTAINER (master_box), (GtkWidget*)button_box);
            gtk_box_set_homogeneous (button_box, TRUE);

            GtkWidget *cancel;
            cancel = gtk_button_new_with_label ("Anuluj");
            g_signal_connect (cancel, "clicked", G_CALLBACK (game_quit), data);
            gtk_container_add (GTK_CONTAINER (button_box), cancel);

            GtkWidget *save;
            save = gtk_button_new_with_label ("Zapisz");
            g_signal_connect (save, "clicked", G_CALLBACK (save_result), data);
            gtk_container_add (GTK_CONTAINER (button_box), save);
        }
        else{
            GtkWidget *close;
            close = gtk_button_new_with_label ("Zamknij");
            g_signal_connect (close, "clicked", G_CALLBACK (game_quit), data);
            gtk_container_add (GTK_CONTAINER (master_box), close);

        }
    }
    if( (data->mode == 'A' || data->mode == 'B') && data->score[0] > data->score[1] ){
        GtkLabel *result_label;
        result_label = (GtkLabel*)gtk_label_new("Niestety przegrałeś");
        gtk_container_add (GTK_CONTAINER (master_box), (GtkWidget*)result_label);

        GtkWidget *close;
        close = gtk_button_new_with_label ("Zamknij");
        g_signal_connect (close, "clicked", G_CALLBACK (game_quit), data);
        gtk_container_add (GTK_CONTAINER (master_box), close);
    }
    if( (data->mode == 'A' || data->mode == 'B') && data->score[0] == data->score[1] ){
        GtkLabel *result_label;
        result_label = (GtkLabel*)gtk_label_new("Remis");
        gtk_container_add (GTK_CONTAINER (master_box), (GtkWidget*)result_label);

        GtkWidget *close;
        close = gtk_button_new_with_label ("Zamknij");
        g_signal_connect (close, "clicked", G_CALLBACK (game_quit), data);
        gtk_container_add (GTK_CONTAINER (master_box), close);
    }

    gtk_container_add (GTK_CONTAINER (window),(GtkWidget*) master_box);

    gtk_widget_show_all ((GtkWidget*)window);
}
