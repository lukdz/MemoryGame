#include <stdlib.h>
#include <math.h>
#include <gtk/gtk.h>
#include <string.h>


#include "game_back_end.h"
#include "game_end_window.h"

#define MAKS_DL_TEKSTU 1000



bool less_than_second_number(int x, int n, int tab[n]){
    int counter=0;
    for(int i=0; i<n; i++){
        if(tab[i]==x)
            counter++;
    }
    if(counter>1){
        return false;
    }
    else{
        return true;
    }
}

void board_generator(int size, int tab[size]){
    int x;

    srand (time(NULL));
    for(int i=0; i<size; i++){
        x = rand() % (size/2);
        if(less_than_second_number(x, i, tab)){
            tab[i] = x;
        }
        else{
            i--;
        }
    }

    printf("board_generator\n");
    for(int i=0; i<size; i++){
        printf("i = %d,\ttab[i] = %d\n", i, tab[i]);
    }
    printf("\n");

}

void board_print(BoardData *data){
    for(int y=0; y<data->size_y; y++){
        for(int x=0; x<data->size_x; x++){
            printf("%d\t", data->tab[(y*data->size_x)+x] );
        }
        printf("\n");
    }
    printf("\n");
}

void clicked_image(BoardData *data){
    int card_number = (data->y*data->size_x) + data->x;

    if( (data->clicked[1] == -1) && (!data->taken[card_number]) ){
        if( data->clicked[0] != -1 )
            data->clicked[1] = card_number;
        else
            data->clicked[0] = card_number;

        gtk_image_set_from_pixbuf (data->card[ card_number ],
                            data->image[ data->tab[ card_number ] ]);
        data->taken[card_number]=true;
    }
    else{
        if( (data->clicked[0] != -1) && (data->clicked[1] != -1) ){
            if( data->tab[ data->clicked[0] ] == data->tab[ data->clicked[1] ] ){
                gtk_image_set_from_pixbuf (data->card[ data->clicked[0] ],
                                            data->blank);
                gtk_image_set_from_pixbuf (data->card[ data->clicked[1] ],
                                            data->blank);
                data->cards_left--;
                if(data->cards_left == 0){
                    printf("results_display\n");
                    results_display(data);
                    return;
                }
                printf("cards_left: %d\n", data->cards_left);
            }
            else{
                gtk_image_set_from_pixbuf (data->card[ data->clicked[0] ],
                                            data->color);
                gtk_image_set_from_pixbuf (data->card[ data->clicked[1] ],
                                            data->color);
                data->taken[ data->clicked[0] ]=false;
                data->taken[ data->clicked[1] ]=false;
                if( data->mode == 'S' || ( (data->mode == 'A' || data->mode == 'B') && data->turn ) ){
                    data->score[0]++;
                    printf("Score +1\n");
                    char score_text[100];
                    sprintf(score_text, "Twój wynik: %d", data->score[0]);
                    gtk_label_set_text( GTK_LABEL(data->label[0]), score_text);
                }
                if( (data->mode == 'A' || data->mode == 'B') && !data->turn ){
                    data->score[1]++;
                    printf("Score +1\n");
                    char score_text[100];
                    sprintf(score_text, "Wynik drugiego gracza: %d", data->score[1]);
                    gtk_label_set_text( GTK_LABEL(data->label[1]), score_text);
                }
            }
            printf("Score: %d\n", data->score[0]);

            data->clicked[0] = -1;
            data->clicked[1] = -1;
            printf("\n");
            data->turn = !data->turn;
        }

    }

}


void button_press_callback (GtkWidget      *event_box,
                            GdkEventButton *event,
                            gpointer       data_in)
{
    BoardData *data = data_in;
    data->x = floor( event->x / (64+5) );
    data->y = floor( event->y / (64+5) );
    g_print ("\nEvent box clicked: X = %d, Y = %d\n",
            data->x, data->y);

    if( data->mode == 'S' ){
        g_print ("Event enabled\n");
        clicked_image(data);
    }
    if( (data->mode == 'A' || data->mode == 'B') && data->turn ){
        g_print ("Event enabled & send\n");

        char clicked_text[MAKS_DL_TEKSTU];
        sprintf(clicked_text, "%d %d", data->y, data->x);
        sprintf(clicked_text+strlen(clicked_text), "\n");
        printf(clicked_text, "clicked_text: ""%d %d""\n", data->y, data->x);
        sendStringToPipe(data->channel, &clicked_text[0]);

        clicked_image(data);
    }

}

gboolean get_second_player_move(gpointer data_temp){
    BoardData *data = data_temp;
    gchar wejscie[MAKS_DL_TEKSTU+5]={0};
    printf("A");
    if( data->quit ){
        return FALSE;
    }

    if (getStringFromPipe(data->channel,wejscie,MAKS_DL_TEKSTU)) {
        if( wejscie[0]=='Q' && data->cards_left > 0 ){
            if ( data->game_window != NULL ){
                gtk_widget_destroy( (GtkWidget*)data->game_window );
                data->game_window = NULL;
                if( data->game_end_window != NULL){
                    gtk_widget_destroy( (GtkWidget*)data->game_end_window );
                    data->game_end_window = NULL;
                }
            }
            closePipes( data->channel );
            free( data );
            return FALSE;
        }
        if( wejscie[0]=='Q' ){
            return FALSE;
        }
        printf("get_second_player_move: ""%s""\n", wejscie);
        sscanf(wejscie, "%d %d", &(data->y), &(data->x));
        clicked_image(data);
    }

    return TRUE;
}


void send_board (BoardData *data){
    printf("send_board start\n");
    char text[MAKS_DL_TEKSTU+5];

    sprintf(text, "%d ", data->icon_set);
    sprintf(text+strlen(text), "%d ", data->size_x);
    sprintf(text+strlen(text), "%d ", data->size_y);

    for (int i=0; i<data->size_full; i++){
        sprintf(text+strlen(text), "%d ", data->tab[i]);
    }
    sprintf(text+strlen(text), "\n");
    printf("SEND: %s", text);
    sendStringToPipe(data->channel, text);
    printf("send_board complited\n\n");
}

bool get_board (BoardData *data){
    char wejscie[MAKS_DL_TEKSTU+5];
    char *poz;
    char space[] = "  ";

    if( getStringFromPipe(data->channel,wejscie ,MAKS_DL_TEKSTU) ){
        printf("Dostano: %s\n", wejscie);

        sscanf(wejscie, "%d", &(data->icon_set));
        printf("icon_set: %d\n", data->icon_set);

        poz = strpbrk(wejscie, space);
        sscanf(poz, "%d ", &(data->size_x));
        printf("size_x: %d\n", data->size_x);

        poz = strpbrk(poz+1, space);
        sscanf(poz, "%d ", &(data->size_y));
        printf("size_y: %d\n", data->size_y);

        data->size_full = data->size_x*data->size_y;
        data->size_half = (data->size_x*data->size_y) / 2;

        //data->cards_left = 1;
        data->cards_left = data->size_half; //TESTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT

        printf("size_full: %d\n", data->size_full);

        for (int i=0; i<data->size_full; i++){
            poz = strpbrk(poz+1 , space);
            sscanf(poz, "%d ", &(data->tab[i]));
            printf("tab[i] get: %d\n", data->tab[i]);
            }
        printf("get_board complited\n\n");
        return true;
    }
    return false;
}

